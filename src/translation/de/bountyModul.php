<?php
/**
 * DE| Deutsche Sprach Datei für das Prämien Modul von LK-Development
 * @copyright LK-Development
 */

return [
    "paymentUnit" => "Punkte",
    "notFound" => "Die Pr&auml;mie wurde leider nicht gefunden.",
    "backToHome" => "Zur&uuml;ck",
    "noBountiesInCategory" => "Leider sind in dieser Kategorie keine Prämien vorhanden. Probieren Sie doch eine unserer anderen Kategorien aus."
];