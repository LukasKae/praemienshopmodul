<?php
Route::get("/", ['uses' => "BountyController@home", 'as' => "bountyHome"]);
Route::get("/{id}-{name}", ['uses' => "BountyController@category", 'as' => "bountyCategoryPrettyHome"]);
Route::get("/cat/{id}", ['uses' => "BountyController@category", 'as' => "bountyCategoryHome"]);
Route::get("/tag/{id}", ['uses' => "BountyController@tag", 'as' => "bountyTagHome"]);
Route::get("/tag/{id}-{name}", ['uses' => "BountyController@tag", 'as' => "bountyPrettyTagHome"]);
Route::get("/i/{id}", ['uses' => "BountyController@show", 'as' => "bountyDisplay"]);
Route::get("/i/{id}-{name}", ['uses' => "BountyController@show", 'as' => "bountyPrettyDisplay"]);
