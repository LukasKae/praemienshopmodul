@extends(config("bountyModul.masterLayout"))

@section(config("bountyModul.contentSection"))
    <div class="alert alert-danger">
        {{trans('bountyModul::bountyModul.notFound')}}
    </div>
    <p><a href="{{route('bountyHome')}}"
          class=" btn btn-danger">{{trans('bountyModul::bountyModul.backToHome')}}</a></p>
@endsection