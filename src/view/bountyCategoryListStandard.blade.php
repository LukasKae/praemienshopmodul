<h3>Unsere Kategorien</h3>
<ul>
    @foreach($_categories as $category)
        <li><a href="{{$category->generateUrl()}}">{{$category->name}}</a></li>
    @endforeach
</ul>