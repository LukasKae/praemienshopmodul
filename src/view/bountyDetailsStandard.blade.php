@extends(config("bountyModul.masterLayout"))

@section(config("bountyModul.contentSection"))
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <h1>{{$bounties[0]->name}}</h1>
                    @if(config("bountyModul.enableTags") == true)
                        <ul class="tags">
                            @foreach($bounties[0]->tags()->get() as $tag)
                                <li><a href="{{ $tag->generateUrl() }}" class="tag">{{$tag->name}}</a></li>
                            @endforeach
                        </ul>
                    @endif
                    <span>{{$bounties[0]->description}} </span>

                    <p><a href="{{route('bountyHome')}}"
                          class=" btn btn-danger">{{trans('bountyModul::bountyModul.backToHome')}}</a></p>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <img src="{{$bounties[0]->image()}}" class="img-responsive"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            @if(count($bounties) > 1)
                                <select id="variantChange" class="form-control">
                                    @foreach($bounties as $bounty)
                                        <option value="{{$bounty->generateUrl()}}"
                                                @if($bounty->id == $id) selected @endif>{{$bounties[0]->name }} {{$bounty->variantTyp}}
                                            | {{$bounty->price}} {{trans('bountyModul::bountyModul.paymentUnit')}}
                                        </option>
                                    @endforeach
                                </select>
                            @else
                                <h2 class="pull-right">{{$bounties[0]->formatPrice($bounties[0]->price,0)}} {{trans('bountyModul::bountyModul.paymentUnit')}}</h2>
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            @foreach($bounties as $bounty)
                                @if($bounty->id == $id)
                                    @include(config('bountyModul.shopBounty'),['_bounty' => $bounty->id])
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        r(function () {
            modul_praemien_load_it();
        });
        function r(f) {
            /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f()
        }
        var modul_praemien_load_it = function () {
            var js = document.createElement("script");

            js.type = "text/javascript";
            js.src = '{{asset("vendor/bountyModul/js/bountyModul.js")}}';

            document.body.appendChild(js);
        };

    </script>
@endsection