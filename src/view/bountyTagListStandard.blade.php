@if(config("bountyModul.enableTags") == true)
    <h3>Tags</h3>
    <ul class="tags">
        @foreach($_tags as $tag)
            <li><a href="{{ $tag->generateUrl() }}" class="tag">{{$tag->name}}</a></li>
        @endforeach
    </ul>
@endif