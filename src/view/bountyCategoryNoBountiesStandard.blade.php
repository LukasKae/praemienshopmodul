<div class="alert alert-danger">
    {{trans('bountyModul::bountyModul.noBountiesInCategory')}}
</div>
<p><a href="{{route('bountyHome')}}"
      class=" btn btn-danger">{{trans('bountyModul::bountyModul.backToHome')}}</a></p>