@extends(config("bountyModul.masterLayout"))

@section(config("bountyModul.contentSection"))
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8 col-xs-8">
            @if($bounties->count() > 0)
            <div class="row">
                @for($i = 0; $i < $bounties->count(); $i++)
                    @if($i % config('bountyModul.bountiesPerRow') === 0)
            </div>
            <div class="row">
                @endif
                <div class="col-md-{{ 12 / config('bountyModul.bountiesPerRow') }} col-lg-{{ 12 / config('bountyModul.bountiesPerRow') }}">
                    @include(config("bountyModul.bountyPreviewView"),["_bounty" => $bounties[$i]])
                </div>
                @endfor
            </div>
                {!! $bounties->render() !!}
            @else
                @include(config('bountyModul.bountyNoBountyInCategoryView'))
            @endif
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
            @include(config("bountyModul.bountyCategoryListView"),["_categories" => $categories])
            @include(config("bountyModul.bountyTagListView"),["_tags" => $all_tags])
        </div>
    </div>
    <script>
        r(function () {
            modul_praemien_load_it();
        });
        function r(f) {
            /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f()
        }
        var modul_praemien_load_it = function () {
            var js = document.createElement("script");

            js.type = "text/javascript";
            js.src = '{{asset("vendor/bountyModul/js/bountyModul.js")}}';

            document.body.appendChild(js);
        };

    </script>
@endsection