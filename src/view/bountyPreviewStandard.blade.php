<div style="border:1px solid #cccccc; margin-bottom: 5px;">
    <div class="row">
        <a href="{{$_bounty->generateUrl()}}">
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                <img src="{{$_bounty->image()}}" alt="{{$_bounty->name}}" class="img-responsive">
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                <h4>{{str_limit($_bounty->name,20)}}</h4>
                <div class="text-info">{{$_bounty->formatPrice($_bounty->price,0)}} {{trans('bountyModul::bountyModul.paymentUnit')}}</div>
                <div class="text-muted">{{ str_limit($_bounty->description,200) }}</div>
                @if(config("bountyModul.enableTags") == true)
                    <ul class="tags">
                        @foreach($_bounty->tags()->take(5)->get() as $tag)
                            <li><a href="{{ $tag->generateUrl() }}" class="tag">{{$tag->name}}</a></li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </a>
    </div>
</div>
