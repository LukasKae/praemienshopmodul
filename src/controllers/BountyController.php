<?php

namespace lukaskae\bountyModul\controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use lukaskae\bountyModul\models\BountyCategory;
use lukaskae\bountyModul\models\Bounty;
use lukaskae\bountyModul\models\BountyTag;

class BountyController extends Controller
{
    use ValidatesRequests;

    /**
     * Display all Bounties and all Categories and all Tags
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $bounties = Bounty::Active()->NoVariant()->paginate(config('bountyModul.bountiesPerPage'));
        $categories = BountyCategory::Active()->get();
        $all_tags = BountyTag::Popularity()->take(10)->get();
        return view(config("bountyModul.bountiesOverviewView"), compact("bounties", "categories", "all_tags"));
    }

    /**
     * Displays all Bounties from this category
     *
     * @param $categoryId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($categoryId)
    {
        $bounties = Bounty::has("categories", "=", $categoryId)->Active()->NoVariant()->paginate(config('bountyModul.bountiesPerPage'));
        $categories = BountyCategory::Active()->get();
        $all_tags = BountyTag::Popularity()->take(10)->get();
        return view(config("bountyModul.bountiesOverviewView"), compact("bounties", "categories", "all_tags"));
    }

    /**
     * Display all Bounties from this Tag
     *
     * @param $tagId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tag($tagId)
    {
        $tag = BountyTag::find($tagId);
        $bounties = $tag->bounties()->Active()->paginate(config('bountyModul.bountiesPerPage'));
        $all_tags = BountyTag::Popularity()->take(10)->get();
        $categories = BountyCategory::Active()->get();
        return view(config("bountyModul.bountiesOverviewView"), compact("bounties", "categories", "all_tags"));
    }
    /**
     * Displays a specific Bounty
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $bounties = Bounty::Active()->WithVariant($id)->get();
        if (empty($bounties)) {
            return view(config("bountyModul.bountyNotFoundView"));
        }
        return view(config("bountyModul.bountyDetailsView"), compact("bounties", "id"));
    }
}