<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Bounty per Page
    |--------------------------------------------------------------------------
    |
    | Configure how many Bounties will displayed of each Page of the Pagination.
    |
    */
    "bountiesPerPage" => 20,
    /*
    |--------------------------------------------------------------------------
    | Bounty per Row
    |--------------------------------------------------------------------------
    |
    | Configure how many Bounties will displayed of each Row of the Overview
    | Page.
    |
    */
    "bountiesPerRow" => 1,
    /*
    |--------------------------------------------------------------------------
    | Route Prefix
    |--------------------------------------------------------------------------
    |
    | Configure the Prefix of the Module Routes are
    |
    */
    "routePrefix" => "bounties/",
    /*
    |--------------------------------------------------------------------------
    | Master Layout
    |--------------------------------------------------------------------------
    |
    | Configure the name of the Master Layout, that Layout will be extended
    | by this Module.
    |
    */
    "masterLayout" => "layout",
    /*
    |--------------------------------------------------------------------------
    | Section Definition
    |--------------------------------------------------------------------------
    |
    | Configure the name of the Section which will be replaced with the Module
    | Views.
    |
    */
    "contentSection" => "content",
    /*
    |--------------------------------------------------------------------------
    | bounties Overview View
    |--------------------------------------------------------------------------
    |
    | Configure the name of the View which represent the Bounty Overview.
    |
    */
    "bountiesOverviewView" => "bountyModul::bountiesOverviewStandard",
    /*
    |--------------------------------------------------------------------------
    | bountiy Category List View
    |--------------------------------------------------------------------------
    |
    | Configure the name of the View which represent the Bounty Overview.
    |
    */
    "bountyCategoryListView" => "bountyModul::bountyCategoryListStandard",
    /*
    |--------------------------------------------------------------------------
    | bountiy Category List View
    |--------------------------------------------------------------------------
    |
    | Configure the name of the View which represent the Bounty Overview.
    |
    */
    "bountyTagListView" => "bountyModul::bountyTagListStandard",
    /*
    |--------------------------------------------------------------------------
    | bounty Preview View
    |--------------------------------------------------------------------------
    |
    | Configure the name of the View which represent one Bountie in the
    | Overview Page.
    |
    */
    "bountyPreviewView" => "bountyModul::bountyPreviewStandard",
    /*
    |--------------------------------------------------------------------------
    | bounty Details View
    |--------------------------------------------------------------------------
    |
    | Configure the name of the View which display one Bounty in the Details
    | Page.
    |
    */
    "bountyDetailsView" => "bountyModul::bountyDetailsStandard",
    /*
   |--------------------------------------------------------------------------
   | bounty Not Found View
   |--------------------------------------------------------------------------
   |
   | Configure the name of the View which display if the Bounty was not found.
   |
   */
    "bountyNotFoundView" => "bountyModul::bountyNotFoundStandard",
    /*
   |--------------------------------------------------------------------------
   | No Bounty in Category
   |--------------------------------------------------------------------------
   |
   | Configure the name of the View which display if the Bounty was not found.
   |
   */
    "bountyNoBountyInCategoryView" => "bountyModul::bountyCategoryNoBountiesStandard",
    /*
    |--------------------------------------------------------------------------
    | Shop Bounty
    |--------------------------------------------------------------------------
    |
    | Configure the name of the view that contains the Shopping Link.
    |
    */
    "shopBounty" => "shop.it",
    /*
    |--------------------------------------------------------------------------
    | Readable URLs
    |--------------------------------------------------------------------------
    |
    | Configure if the URls of the Categories must be readable.
    | False if you want an URL like: www.example.com/routePrefix/cat/1337
    | True if you want an URL like: www.example.com/routePrefix/1337-the-best-category-ever
    | If True you must add this line to your .htaccess
    | Rewrite Rule
    |
    |
    */
    "readableUrls" => true,
    /*
   |--------------------------------------------------------------------------
   | Enable Tags
   |--------------------------------------------------------------------------
   |
   | Configure if you want to enable Tags in the Systeme
   |
   */
    "enableTags" => true,

];

