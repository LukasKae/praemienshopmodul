<?php

namespace lukaskae\bountyModul\models;

use Illuminate\Database\Eloquent\Model;
use lukaskae\praemienModul\controllers\BountyController;
use Illuminate\Support\Facades\DB;
class BountyTag extends Model
{
    /**
     * Table is lk_bountyTags
     *
     * @var string
     */
    protected $table = 'lk_bountyTags';

    public function bounties()
    {
        return $this->belongsToMany(Bounty::class, "lk_bountyToTag", "bountyTag_id", "bounty_id");
    }

    public function scopePopularity($query)
    {
        /* $query->select(self::getTable() . ".id", self::getTable() . ".name", DB::raw("COUNT(t2.name) as counter"))
             ->join(self::getTable() . " as t2", self::getTable() . ".name", "=", "t2.name")
             ->groupBy(self::getTable() . ".name")
             ->orderBy("counter", 'DESC'); */
        $query->select(self::getTable() . ".*", DB::raw("COUNT(lk_bountyToTag.bountyTag_id) as counter"))
            ->join("lk_bountyToTag", "bountyTag_id", "=", self::getTable() . ".id")
            ->where(DB::raw("(SELECT count(*) FROM `lk_bountyToTag` WHERE bountyTag_id = `lk_bountyTags`.`id`)"), ">", 0)
            ->groupBy("lk_bountyToTag.bountyTag_id")
            ->orderBy("counter", 'DESC');
    }
    /**
     * @return string
     */
    public function generateUrl()
    {
        if (config("bountyModul.readableUrls") == true) {
            $url = route('bountyPrettyTagHome', ["id" => $this->id, "name" => str_slug($this->name)]);
        } else {
            $url = route('bountyTagHome', ["id" => $this->id]);
        }
        return $url;
    }
}
