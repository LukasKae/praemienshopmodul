<?php

namespace lukaskae\bountyModul\models;

use Illuminate\Database\Eloquent\Model;
use lukaskae\praemienModul\controllers\BountyController;

class Bounty extends Model
{
    /**
     * Table is lk_praemien
     *
     * @var string
     */
    protected $table = 'lk_bounties';

    /**
     * Displays only Active bounties
     *
     * @param $query
     */
    public function scopeActive($query)
    {
        $query->where("active", "=", "Yes");
    }

    /**
     * Display Bounties only if they aren't a Variant
     *
     * @param $query
     *
     */
    public function scopeNoVariant($query)
    {
        $query->whereNull("variantOf");
    }

    /**
     * Display Bounties with Variants
     *
     * @param $query
     * @param $pid
     */
    public function scopeWithVariant($query, $pid)
    {
        $praemie = self::find($pid);
        if ($praemie->variantOf == null) {
            $query->where(function ($query2) use ($praemie) {
                $query2->where("id", "=", $praemie->id)->orWhere("variantOf", "=", $praemie->id);
            });
        } else {
            $query->where(function ($query2) use ($praemie) {
                $query2->where("id", "=", $praemie->variantOf)->orWhere("id", "=", $praemie->id)->orWhere("variantOf", "=", $praemie->variantOf);
            });

        }

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(BountyCategory::class, "lk_bountyToCategory", "bounty_id", "bountyCategory_id");
    }

    public function tags()
    {
        return $this->belongsToMany(BountyTag::class, "lk_bountyToTag", "bounty_id", "bountyTag_id");
    }
    /**
     * Returns all Categories from this Bounty
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCategories()
    {
        return $this->categories()->get();
    }
    /**
     * Format the Price with number_format
     *
     * @param $number
     * @param int $decimals
     * @param string $dec_point
     * @param string $thousands_sep
     * @return string
     */
    public function formatPrice($number, $decimals = 0, $dec_point = ".", $thousands_sep = ",")
    {
        return number_format($number, $decimals, $dec_point, $thousands_sep);

    }

    /**
     * Check if is an Variant of an other Product
     *
     * @return bool
     */
    public function isVariant()
    {
        return ($this->variantOf == null) ? false : true;
    }
    /**
     * Return the URL to the Images of the Bounty
     *
     * @return mixed
     */
    public function image()
    {
        $id = $this->id;
        if (!file_exists(public_path("pic/$id.jpg"))) {
            return asset("vendor/bountyModul/pic/0.jpg");
        } else {
            return asset("vendor/bountyModul/pic/$id.jpg");
        }
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        if (config("bountyModul.readableUrls") == true) {
            if ($this->isVariant()) {
                $master = self::find($this->variantOf);
                $this->name = $master->name . "-" . $this->variantTyp;
            }
            $url = route('bountyPrettyDisplay', ["id" => $this->id, "name" => str_slug($this->name)]);
        } else {
            $url = route('bountyDisplay', ["id" => $this->id]);
        }
        return $url;
    }
}
