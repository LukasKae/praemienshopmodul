<?php

namespace lukaskae\bountyModul\models;

use Illuminate\Database\Eloquent\Model;

class BountyCategory extends Model
{
    /**
     * Table is lk_praemien
     *
     * @var string
     */
    protected $table = 'lk_bountyCategory';

    /**
     * Displays only Active Categories
     *
     * @param $query
     */
    public function scopeActive($query)
    {
        $query->where("active", "=", "Yes");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function bounties()
    {
        return $this->belongsToMany(Bounty::class, "lk_bountyToCategory", "bountyCategory_id", "bounty_id");
    }

    /**
     * Return all Bounties from this Category
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBounties()
    {
        return $this->bounties()->get();
    }

    /**
     * @return string
     */
    public function generateUrl()
    {
        if (config("bountyModul.readableUrls") == true) {
            $url = route('bountyCategoryPrettyHome', ["id" => $this->id, "name" => str_slug($this->name)]);
        } else {
            $url = route('bountyCategoryHome', ["id" => $this->id]);
        }
        return $url;
    }
}
