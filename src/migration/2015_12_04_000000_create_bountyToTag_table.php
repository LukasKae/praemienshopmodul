<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBountyToTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_bountyToTag', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('bounty_id')->unsigned();
            $table->integer('bountyTag_id')->unsigned();
            $table->foreign('bounty_id')->references('id')->on('lk_bounties');
            $table->foreign('bountyTag_id')->references('id')->on('lk_bountyTags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lk_bountyToTag');
    }
}
