<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBountyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_bounties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->decimal('price', 5, 2)->default(0);
            $table->enum('active', ["Yes", "No"])->default('No');
            $table->string('itemNumber');
            $table->string('variantTyp')->comment('What is the Different between other Variants of this Type?');
            $table->integer('variantOf')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lk_bounties');
    }
}
