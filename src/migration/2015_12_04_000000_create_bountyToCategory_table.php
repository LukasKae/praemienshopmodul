<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBountyToCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_bountyToCategory', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('bounty_id')->unsigned();
            $table->integer('bountyCategory_id')->unsigned();
            $table->foreign('bounty_id')->references('id')->on('lk_bounties');
            $table->foreign('bountyCategory_id')->references('id')->on('lk_bountyCategory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lk_bountyToCategory');
    }
}
