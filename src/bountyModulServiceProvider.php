<?php
namespace lukaskae\bountyModul;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

class bountyModulServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Boot Parent Service Provider
         */
        parent::boot();
        $this->mergeConfigFrom(
            __DIR__ . '/config/bountyModul.php', 'bountyModul'
        );
        /**
         *  Check if Routes Are Cached
         */
        if (!$this->app->routesAreCached()) {
            /**
             *  If Routes aren't cached register Routes
             */
            Route::group(["prefix" => config("bountyModul.routePrefix"), "namespace" => 'lukaskae\bountyModul\controllers'], function () {
                require __DIR__ . '/routes.php';
            });
        }

        /**
         * Define Dir where Views From
         */
        $this->loadViewsFrom(__DIR__ . '/view', 'bountyModul');
        /**
         * Publishes all subjects
         */
        $this->publishes([
            __DIR__ . '/assets' => public_path('vendor/bountyModul'),
            __DIR__ . '/migration' => database_path('migrations'),
            __DIR__ . '/config/bountyModul.php' => config_path('bountyModul.php'),
            __DIR__ . '/translation' => base_path('resources/lang/bountyModul'),
        ], 'praemienModul');
        $this->loadTranslationsFrom(__DIR__ . '/translation', 'bountyModul');

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        //$this->app->make('Bernardino\SimpleAdmin\SimpleAdminController');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {

    }

    /**
     * Get the active router.
     *
     * @return Router
     */
    protected function getRouter()
    {
        return $this->app['router'];
    }
}